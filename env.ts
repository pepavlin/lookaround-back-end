import { TypeOrmModuleOptions } from "@nestjs/typeorm";

export const ENV : {DB: TypeOrmModuleOptions} = {
    DB:{
        type: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'lookaround',
        password: '31663lookaround',
        database: 'lookaround',
        synchronize: false,
    }
};
