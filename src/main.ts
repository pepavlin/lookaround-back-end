import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  (app as any).httpAdapter.instance.set('json spaces', 2);
  await app.listen(3000);
  console.log(`Server is running on ${await app.getUrl()}`);
}
bootstrap();
