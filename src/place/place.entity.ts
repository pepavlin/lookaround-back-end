import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("places")
export class Place{
    @PrimaryGeneratedColumn()
    id : number;

    @Column()
    name:string;

    @Column()
    type: string;
    
    @Column({type:"text"})
    description : string;
    
    @Column({type:"text", nullable:true})
    url : string;

    @Column({type:"text", nullable:true})
    imageUrl : string;

    @Column({type:"simple-json", nullable:true})
    images : any
    
    @Column({type:"float"})
    latitude : number;
    
    @Column({type:"float"})
    longitude : number;
    
    @Column({type:"float"})
    altitude : number;
    
    @Column()
    batch: string
}