import { Controller, Get, Param, Query } from "@nestjs/common";
import { text } from "stream/consumers";
import { PlacesService } from "./places.service";
import Utils from "../Utils"

@Controller("places")
export class PlacesController{
    constructor(private readonly placesService: PlacesService){}
    

    @Get("chunk")
    async getByChunk(@Query('x') x, @Query('y') y, @Query('size') size) : Promise<object>{
        
        const meters = [x*size, y*size];
        const latitude = Utils.metersToDegrees(meters[1],0).lat;
        const longitude = Utils.metersToDegrees(meters[0],latitude).lng;

        const text = await this.placesService.findInRect(latitude, longitude, size, size);
        return text;
    }

    @Get("version")
    async getVersion() : Promise<object>{
        return {version: "baseversion"};
    }

}