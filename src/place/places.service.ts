import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import Utils from "src/Utils";
import { LessThan, Repository } from "typeorm";
import { Place } from "./place.entity";

@Injectable()
export class PlacesService {
  constructor(
    @InjectRepository(Place)
    private placesRepository: Repository<Place>,
  ) {}

  findInRect(latitude:number, longitude:number, width:number, height:number) : Promise<Place[]>{

    const dw = Utils.metersToDegrees(width, latitude).lng/2;
    const dh = Utils.metersToDegrees(height, latitude).lat/2;
    const latMin :number = latitude-dh;
    const latMax :number= latitude+dh;
    const lonMin :number= longitude-dw;
    const lonMax :number = longitude+dw;
    
    return this.placesRepository.createQueryBuilder()
      .where("latitude BETWEEN :minLat AND :maxLat", { minLat:latMin, maxLat:latMax})
      .andWhere("longitude BETWEEN :minLon AND :maxLon", { minLon:lonMin, maxLon:lonMax})
      //.andWhere("batch in (:batchName)", {batchName: ["drobnepamatkyWithImage","manual"]})
      .limit(100)
      .getMany();
  }


}