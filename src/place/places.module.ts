import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Place } from "./place.entity";
import { PlacesController } from "./places.controller";
import { PlacesService } from "./places.service";

@Module({
    imports: [
        TypeOrmModule.forFeature([Place])
    ],
    providers: [PlacesService],
    exports: [PlacesService],
    controllers: [PlacesController]
})
export class PlaceModule{

}