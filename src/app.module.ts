import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Place } from './place/place.entity';
import { PlaceModule } from './place/places.module';

import {ENV} from "../env"

@Module({
  imports: [
      PlaceModule,
      /*TypeOrmModule.forRoot({
        type: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'lookaround',
        password: '31663lookaround',
        database: 'lookaround',
        entities: [Place],
        synchronize: false,
      })*/
      TypeOrmModule.forRoot({
        ...ENV.DB, 
        entities: [Place],
      })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
