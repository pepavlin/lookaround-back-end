class Utils{
    degreesToMeters = (degrees: {lat:number, lng: number}) : number[] => {
        const result = [0,0];
        result[0] = degrees.lat*111111;
        result[1] = degrees.lat*(111111*Math.cos(degrees.lat));

        return result;
    }

    metersToDegrees = (meters: number, lat: number) : {lat: number, lng: number} =>{
        const result = {lat:0, lng:0};

        result.lat = (meters/111111);
        result.lng = (meters/(111111*Math.cos(lat)));

        return result;
    }
}
export default new Utils();