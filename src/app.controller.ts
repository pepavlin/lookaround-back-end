import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return "Nothing here";
  }
  @Get("Test")
  getTest(): string {
    return "This is test text";
  }
}
