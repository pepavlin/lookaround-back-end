FROM node:12.19.0-alpine3.9

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

ENV PORT=3000
EXPOSE 8080
EXPOSE 3306

CMD ["npm", "start"]